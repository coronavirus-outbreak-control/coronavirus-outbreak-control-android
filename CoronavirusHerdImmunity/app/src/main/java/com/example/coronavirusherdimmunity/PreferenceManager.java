package com.example.coronavirusherdimmunity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import com.example.coronavirusherdimmunity.utils.PatientStatusParse;

import org.coronavirus_outbreak_control.android.sdk.enums.ApplicationStatus;

public class PreferenceManager {

    private final SharedPreferences pref;

    // Shared preferences file names
    private static final String PREF_NAME = "WelcomeActivity";
    private static final String PREF_NAME_SDK_VERS = "SdkVersion";  //used to save sdk versions

    /***** Key shared preferences "WelcomeActivity" *******/
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String PATIENT_STATUS = "patientStatus";
    private static final String APPLICATION_STATUS = "applicationStatus";
    private static final String USER_LOCATION_PERMISSION = "userLocationPermission";

    /***** Key shared preferences "WelcomeActivity" *******/
    private static final String SDK_VERS = "sdk_vers"; //sdk android version

    public PreferenceManager(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        updateSDKVersion(context);   //save new sdk version
    }

    /******** functions on Shared Preference "WelcomeActivity" *************/

    public void setFirstTimeLaunch(boolean isFirstTime) {
        pref.edit().putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime).apply();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    // {0: normal, 1: infected, 2: quarantine, 3: healed, 4: suspect}
    public void setPatientStatus(PatientStatusParse status) {
        setPatientStatus(status.toInt());
    }

    public void setPatientStatus(int status) {
        pref.edit().putInt(PATIENT_STATUS, status).apply();
    }

    public PatientStatusParse getPatientStatus() {
        return PatientStatusParse.valueOf(pref.getInt(PATIENT_STATUS, 0));
    }

    // {0: active, 1: inactive}
    public void setApplicationStatus(ApplicationStatus status) {
        setApplicationStatus(status.toInt());
    }

    public void setApplicationStatus(int status) {
        pref.edit().putInt(APPLICATION_STATUS, status).apply();
    }

    public ApplicationStatus getApplicationStatus() {
        return ApplicationStatus.valueOf(pref.getInt(APPLICATION_STATUS, 0));
    }

    public void setUserLocationPermission(boolean userLocationPermission) {
        pref.edit().putBoolean(USER_LOCATION_PERMISSION, userLocationPermission).apply();
    }

    /******** functions on Shared Preference "SdkVersion" *************/

    private void updateSDKVersion(Context context) {
        SharedPreferences sharedPreferenceSdkVersion = context.getSharedPreferences(PREF_NAME_SDK_VERS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferenceSdkVersion.edit();
        editor.putInt(SDK_VERS, Build.VERSION.SDK_INT);
        editor.apply();
    }

}