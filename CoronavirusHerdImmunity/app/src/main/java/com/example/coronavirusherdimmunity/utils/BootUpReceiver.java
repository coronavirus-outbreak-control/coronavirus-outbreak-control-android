package com.example.coronavirusherdimmunity.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.coronavirusherdimmunity.CovidApplication;

public class BootUpReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        CovidApplication.getInstance().initBeaconService().subscribe();
    }
}
