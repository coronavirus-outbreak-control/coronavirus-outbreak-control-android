package com.example.coronavirusherdimmunity.utils;

import com.example.coronavirusherdimmunity.CovidApplication;
import com.example.coronavirusherdimmunity.R;

import org.coronavirus_outbreak_control.android.sdk.enums.PatientStatus;

public enum PatientStatusParse {
    //{0: normal, 1: infected, 2: healed, 3: suspect, 4: lowRisk, 5: mediumRisk, 6: highRisk}
    NORMAL(0, R.string.status_normal, R.string.title_normal, R.string.description_normal, R.color.normal),
    INFECTED(1, R.string.status_infected, R.string.title_infected, R.string.description_infected, R.color.normal),
    SUSPECT(2, R.string.status_suspect, R.string.title_suspect, R.string.description_suspect, R.color.orange),
    HEALED(3, R.string.status_healed, R.string.title_healed, R.string.description_healed, R.color.green),
    LOW_RISK(4, R.string.status_lowrisk, R.string.title_lowrisk, R.string.description_lowrisk, R.color.lightblue),
    MEDIUM_RISK(5, R.string.status_mediumrisk, R.string.title_mediumrisk, R.string.description_mediumrisk, R.color.yellow),
    HIGH_RISK(6, R.string.status_highrisk, R.string.title_highrisk, R.string.description_highrisk, R.color.orange);

    private int intValue;
    private int stringValue;
    private int titleValue;
    private int descriptionValue;
    private int colorValue;

    PatientStatusParse(int value, int toString, int title, int description, int color) {
        intValue = value;
        stringValue = toString;
        titleValue = title;
        descriptionValue = description;
        colorValue = color;
    }

    public static PatientStatusParse valueOf(int value) {
        for (PatientStatusParse p : values()) {
            if (p.intValue == value)
                return p;
        }
        return PatientStatusParse.NORMAL;
    }

    public static PatientStatusParse toParse(PatientStatus patientStatus) {
        for (PatientStatusParse p : values()) {
            if (p.intValue == patientStatus.getInt())
                return p;
        }
        return null;
    }

    public int toInt() {
        return intValue;
    }

    @Override
    public String toString() {
        return CovidApplication.getInstance().getResources().getString(stringValue);
    }

    public String getTitle() {
        return CovidApplication.getInstance().getResources().getString(titleValue);
    }

    public String getDescription() {
        return CovidApplication.getInstance().getResources().getString(descriptionValue);
    }

    public int getColorValue() {
        return colorValue;
    }

    public int getColor() {
        return CovidApplication.getInstance().getResources().getColor(colorValue);
    }

}

