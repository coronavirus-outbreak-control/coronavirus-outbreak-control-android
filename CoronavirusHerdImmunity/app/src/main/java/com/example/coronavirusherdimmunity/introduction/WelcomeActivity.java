package com.example.coronavirusherdimmunity.introduction;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coronavirusherdimmunity.CovidApplication;
import com.example.coronavirusherdimmunity.HowItWorksActivity;
import com.example.coronavirusherdimmunity.R;
import com.example.coronavirusherdimmunity.models.TouchListener;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class WelcomeActivity extends AppCompatActivity {

    private RelativeLayout progBar;

    private Button start_button;
    private Button how_it_works_button;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        setContentView(R.layout.intro0_welcome);


        this.writeTitle();

        progBar = (RelativeLayout) findViewById(R.id.rel_progbar);
        progBar.setVisibility(View.GONE);  //set invisible the relative layout (progress bar + text view)

        start_button = (Button) findViewById(R.id.button_next);
        TouchListener.buttonClickEffect(start_button);   //add click button effect
        start_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onClick_registerDevice();    //if reCaptcha has been success -> register the device and go to next activity
            }
        });

        how_it_works_button = (Button) findViewById(R.id.how_it_works);
        TouchListener.buttonClickEffect(how_it_works_button);   //add click button effect
        how_it_works_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WelcomeActivity.this, HowItWorksActivity.class));
            }
        });

    }


    private void writeTitle() {
        TextView t = (TextView) findViewById(R.id.welcome_to);
        Spanned text = Html.fromHtml(getResources().getString(R.string.welcome));
        t.setText(text);
    }


    /**
     * Manage click on reCaptcha and register device
     * If reCaptcha has been success then the device is registered and go to next activity
     */
    public void onClick_registerDevice() {
        if (CovidApplication.getInstance().getDeviceID() == -1) {
            initBeacon();
        } else {  //else if 'device id' has been already known then go to next activity
            startActivity(new Intent(WelcomeActivity.this, BluetoothActivity.class));    //go to bluetooth activity
        }
    }

    private void initBeacon() {
        Disposable disposable = CovidApplication.getInstance()
                .initBeaconService()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    startActivity(new Intent(WelcomeActivity.this, BluetoothActivity.class));
                }, Throwable::printStackTrace);
        compositeDisposable.add(disposable);
    }
}