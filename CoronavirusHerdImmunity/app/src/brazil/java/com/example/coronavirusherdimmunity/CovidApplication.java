package com.example.coronavirusherdimmunity;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.text.HtmlCompat;

import com.example.coronavirusherdimmunity.utils.PatientStatusParse;

import org.coronavirus_outbreak_control.android.sdk.CovidApplicationSDK;
import org.coronavirus_outbreak_control.android.sdk.enums.ApplicationStatus;
import org.coronavirus_outbreak_control.android.sdk.enums.LoggingMode;
import org.coronavirus_outbreak_control.android.sdk.enums.PatientStatus;

public class CovidApplication extends CovidApplicationSDK {

    private final String TAG = "DemoApplicationLogger";

    private ApplicationStatus lastAppStatus = ApplicationStatus.ACTIVE;
    private PatientStatusParse lastStatus = PatientStatusParse.NORMAL;
    private PreferenceManager preferenceManager;
    private static CovidApplication instance;

    public static CovidApplication getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    public LoggingMode loggingMode() {
        return LoggingMode.SDK_INFORMATION;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        preferenceManager = new PreferenceManager(getApplicationContext());
        lastAppStatus = preferenceManager.getApplicationStatus();
        lastStatus = preferenceManager.getPatientStatus();
    }

    @Override
    public void onPatientStatusUpdated(PatientStatus patientStatus) {
        lastStatus = PatientStatusParse.toParse(patientStatus);
        if (lastStatus != null)
            preferenceManager.setPatientStatus(lastStatus);
        else
            preferenceManager.setPatientStatus(patientStatus.getInt());

        Log.d(TAG, "onPatientStatusUpdated " + patientStatus.name());
        updatePermanentNotification(permanentNotificationBuilder());
    }

    @Override
    public void onApplicationStatusUpdated(ApplicationStatus applicationStatus) {
        lastAppStatus = applicationStatus;
        preferenceManager.setApplicationStatus(lastAppStatus);
        Log.d(TAG, "onApplicationStatusUpdated " + applicationStatus.name());
        updatePermanentNotification(permanentNotificationBuilder());
    }

    @Override
    public Notification.Builder permanentNotificationBuilder() {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.ic_notification);
        builder.setContentTitle(
                HtmlCompat.fromHtml(
                        String.format(getString(R.string.permanent_notification), readableName(lastAppStatus), lastStatus.getTitle()),
                        HtmlCompat.FROM_HTML_MODE_COMPACT));
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT
        );
        builder.setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("FOREGROUNDBEACON",
                    "Foreground beacon service", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Foreground beacon service");
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.enableLights(false);
            channel.enableVibration(false);
            channel.setShowBadge(false);
            channel.setSound(null, null);

            NotificationManager notificationManager = (NotificationManager) getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(channel.getId());
        }
        return builder;
    }


    @Override
    public void onPermissionStatusChanged(int status) {
        Log.d(TAG, "Permission status changed " + status);
        updatePermanentNotification(permanentNotificationBuilder());
    }

    @Override
    public String getApiUrl() {
        return "http://api.covidapp.ufsc.br";
    }

    @Override
    public String getSafetyNetApiSiteKey() {
        return "6LcVVfcUAAAAAKmFmIP8RQ3ZqZ3GdeSgkJq_O_jy";
    }

    private String readableName(ApplicationStatus applicationStatus) {
        switch (applicationStatus) {
            case ACTIVE:
                return getString(R.string.status_active);
            case INACTIVE:
            default:
                return getString(R.string.status_inactive);
        }
    }
}
